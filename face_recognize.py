import vid_capture
from keras import backend as K
'''
import time
from multiprocessing.dummy import Pool
'''

import cv2
import os
import glob
import numpy as np
from numpy import genfromtxt
import tensorflow as tf
from fr_utils import *
from inception_network import *
from face_functions import *
from keras.models import load_model
import sys 



def triplet_loss_function(y_true,y_pred,alpha = 0.3):
    anchor = y_pred[0]
    positive = y_pred[1]
    negative = y_pred[2]
    pos_dist = tf.reduce_sum(tf.square(tf.subtract(anchor, positive)), axis=-1)
    neg_dist = tf.reduce_sum(tf.square(tf.subtract(anchor, negative)), axis=-1)
    basic_loss = tf.add(tf.subtract(pos_dist, neg_dist), alpha)
    loss = tf.reduce_sum(tf.maximum(basic_loss, 0.0))
    return loss


if __name__=='__main__':
    #detector = MTCNN()
    vid_capture.capture()
    cv2.destroyAllWindows()
    #input("skjvbsjkdfbk")
    print("model initializing")
    K.set_image_data_format('channels_first')
    model = model(input_shape = (3,96,96))
    model.compile(optimizer = 'adam', loss = triplet_loss_function, metrics = ['accuracy'])
    load_weights_from_FaceNet(model) 
    database = prepare_database(model)
    #input("model loaded successfuly")
    face = recognise_face("/Users/prituldave/pritul_E_drive/SIH/1.png", database, model)
    input("recognition done")
    print(face)
    #os.remove("1.png")