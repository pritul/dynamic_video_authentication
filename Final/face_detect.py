from mtcnn.mtcnn import MTCNN
import cv2
import numpy as np
import math
#path="/SIH"
def extract_face(img,coord):
	img = img[coord[1]:coord[3],coord[0]:coord[2]]
	cv2.imwrite("1.png",img)

def calc_angle(l):
	
	x1=l[0][0];y1=l[0][1];x2=l[1][0];y2=l[1][1]
	
	slope = (y2-y1)/(x2-x1)
	angle = math.degrees(math.atan(slope))
	return angle

def align_face(img,keypoints):
	img = cv2.imread("1.png")
	img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
	detector = MTCNN()
	results = detector.detect_faces(img)
	print(results)
	img = cv2.circle(img,results[0]['keypoints']['left_eye'],3,(0,0,255),5)
	img = cv2.circle(img,results[0]['keypoints']['right_eye'],3,(0,0,255),5)
	img = cv2.line(img,results[0]['keypoints']['left_eye'],results[0]['keypoints']['right_eye'],(0,255,0),5)
	angle = calc_angle([results[0]['keypoints']['left_eye'],results[0]['keypoints']['right_eye']])
	
	eyesCenter = ((results[0]['keypoints']['left_eye'][0] + results[0]['keypoints']['right_eye'][0]) // 2,(results[0]['keypoints']['left_eye'][1] + results[0]['keypoints']['right_eye'][1]) // 2)
	print(eyesCenter)
	
	M = cv2.getRotationMatrix2D(eyesCenter, angle, 1)
	img=cv2.warpAffine(img, M, img.shape[1::-1], flags=cv2.INTER_LINEAR)
	#print(angle)
	#input('eye center')
	cv2.imshow("image",img)

	cv2.waitKey(0)
	cv2.destroyAllWindows()
	print(results)
	return

def face_detect(img):
	img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
	detector = MTCNN()
	results = detector.detect_faces(img)
	if len(results) == 0 :
		return
	if results[0]['confidence'] >= 0.5:
		print(results)
		x1, y1, width, height = results[0]['box']
		x1, y1 = abs(x1), abs(y1)
		x2, y2 = x1 + width, y1 + height
		img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
		extract_face(img,[x1,y1,x2,y2])
		#test=align_face(img,results[0]['keypoints'])
		img = cv2.rectangle(img,(x1,y1),(x2,y2),(255,0,0),4)
	else:
		print("@debug","unable to detect face check confidence ratio and image")
		input()
	return img